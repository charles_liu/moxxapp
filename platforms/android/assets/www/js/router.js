define(["jquery", "underscore", "backbone",
    "models/MyModel",
    "collections/ProfileCollection",
    "views/StructureView",
    "views/homeView",
    "views/loginView",
    "views/profileView",
    "views/accountView",
    "views/connectionsView"
],
    function ($, _, Backbone, MyModel, ProfileCollection, StructureView, homeView, loginView, profileView, accountView,
        connectionsView) {

        var AppRouter = Backbone.Router.extend({

            routes: {
                "": "showStructure",
                "home": "homeView",
                "login": "loginView",
                "profile/:id" : "profileView",
                "account" : "accountView",
                "following/:id" : "followingView",
                "follower/:id" : "followerView"
            },

            m_myModel: new MyModel({}),

            initialize: function (options) {
                this.currentView = undefined;
            },

            followingView: function(id) {
                this.connectionsView('getFollowing/' + id);
            },
            followerView: function(id) {
                this.connectionsView('getFollowers/' + id);
            },

            connectionsView: function(urlPath) {
                var connectionCollection = new ProfileCollection();
                connectionCollection.configRequest({
                    urlPath: urlPath,
                    loginToken: this.m_myModel.authModel.get("loginToken"),
                    userId: this.m_myModel.authModel.get("userId")
                });
                var page = new connectionsView({collection: connectionCollection});
                this.changePage(page);
                this.structureView.showBack(true, "Back");
            },

            homeView: function () {
                // create a model with an arbitrary attribute for testing the template engine
                // create the view
                var page = new homeView({
                    model: this.m_myModel
                });
                // show the view
                this.changePage(page);
            },

            loginView: function () {
                // create the view and show it
                var page = new loginView({model: this.m_myModel});
                this.changePage(page);
            },

            profileView: function(id) {
                var page = new profileView({attributes: {id: id}, model: this.m_myModel});
                this.changePage(page);
            },

            accountView: function() {
                var page = new accountView();
                this.changePage(page);
            },

            // load the structure view
            showStructure: function () {
                if (!this.structureView) {
                    this.structureView = new StructureView({model: this.m_myModel});
                    // put the el element of the structure view into the DOM
                    document.getElementsByTagName('body')[0].appendChild(this.structureView.render().el);
                }

                // go to first view
                if (this.m_myModel.authModel.hasLogin())
                    this.homeView();
                else
                    this.loginView();
            }

        });

        return AppRouter;

    });