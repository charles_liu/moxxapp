define(["jquery", "underscore", "backbone", "handlebars",
    "models/AuthModel",
    "text!templates/login.html"
],
    function ($, _, Backbone, Handlebars, AuthModel, template) {

        var loginView = Backbone.View.extend({

            id: "loginView",

            events: {
                "click #submitLogin": "submitLogin"
            },

            initialize: function (options) {
            },

            template: Handlebars.compile(template),

            render: function () {
                $(this.el).html(this.template({}));
                return this;
            },

            submitLogin: function (event) {
                event.preventDefault();
                var username = $('#usernameInput').val();
                var password = $('#passwordInput').val();
                username = "charles";
                password = "a";
                if (username.length > 0 && password.length > 0) {
                    this.model.authModel.configRequest(username, password);
                    this.model.authModel.fetch({
                        "success": function(model, response, options) {
                            if (response['success'])
                                Backbone.history.navigate("home", {
                                    trigger: true
                                });
                        },
                        "error": function(model, response, options) {
                            if (navigator.connection.type == Connection.NONE)
                                navigator.notification.alert('No internet connection found.', function() {}, "Login Failed");
                            else
                                navigator.notification.alert('Could not log in. Please verify your login details and try again.', function() {}, "Login Failed");
                        }
                    });
                }
                else {
                    navigator.notification.alert('Login details are missing', function(){}, "Login Details Missing")
                }
            }
        });

        return loginView;

    });