define(["jquery", "underscore", "backbone", "handlebars",
    "models/AuthModel",
    "text!templates/structure.html"
],
    function ($, _, Backbone, Handlebars, AuthModel, template) {

        var StructureView = Backbone.View.extend({

            id: "main",

            events: {
                "click #navProfile" : "profileTouch",
                "click #navAccount" : "accountTouch",
                "click #navHome" : "homeTouch",
                "click #navLogout" : "logoutTouch",
                "click #navBack" : "backTouch"
            },

            initialize: function () {
                // bind the back event to the goBack function
                //document.getElementById("back").addEventListener("back", this.goBack(), false);
                this.listenTo(this.model.authModel, 'change', this.menuHTML);
            },

            template: Handlebars.compile(template),

            render: function () {
                // load the template
                this.el.innerHTML = this.template({showBack: false, showTitle: "", loggedIn: this.model.authModel.hasLogin()});
                // cache a reference to the content element
                this.contentElement = this.$el.find('#content')[0];
                //touchScroll(this.contentElement);
                // if(device.platform == "iOS") {
                //   this.contentElement.parentNode.style.marginTop = "20px";
                //   this.contentElement.parentNode.style.height = "calc(100% - 20px)";
                // }
                return this;
            },

            menuHTML: function() {
                this.partialRender(this.template({loggedIn: this.model.authModel.hasLogin()}), ['#showMenu']);
            },

            // generic go-back function
            goBack: function () {
                //window.history.back();
            },

            profileTouch: function (event) {
                Backbone.history.navigate("profile/" + this.model.authModel.get('userId'), {
                    trigger: true
                });
            },

            accountTouch: function (event) {
                Backbone.history.navigate("account", {
                    trigger: true
                });
            },

            homeTouch: function (event) {
                if (this.model.authModel.hasLogin())
                    Backbone.history.navigate("home", {
                        trigger: true
                    });
                else
                    Backbone.history.navigate("", {
                        trigger: true
                    });
            },
            logoutTouch: function(event) {
                this.model.authModel.logout();
                Backbone.history.navigate("login", {
                    trigger: true
                });
            },

            backTouch: function(event) {
                Backbone.history.history.back();
            },

            showBack: function(isShow, title) {
                this.partialRender(this.template({showBack: isShow, showTitle: title, loggedIn: this.model.authModel.hasLogin()}), '#homeDiv');
            }
        });

        return StructureView;

    });