define(["jquery", "underscore", "backbone", "handlebars",
    "models/ProfileModel",
    "collections/EventCollection",
    "text!templates/profile.html",
    "views/eventItemView",
    "utils/handlebarHelpers"
],
    function ($, _, Backbone, Handlebars, ProfileModel, EventCollection, template, eventItemView) {

        var profileView = Backbone.View.extend({
            showInfo: true,
            eventCount: 0,
            events: {
                "click #getFollowing" : "getFollowing",
                "click #getFollower" : "getFollower",
                "click .toggleTab" : "toggleTab",
                "click #loadMore" : "loadMore"
            },

            initialize: function () {
                this.profModel = this.model;
                if (this.model.authModel.get('userId') != this.attributes.id){
                    this.profModel = new ProfileModel({});
                    this.profModel.config(this.attributes.id, this.model.authModel.get('userId'), this.model.authModel.get('loginToken'));
                    this.profModel.fetch();
                }
                this.eventCollection = new EventCollection();
                this.eventCollection.configRequest({
                    urlPath: "getEvents/" + this.attributes.id + "/",
                    loginToken: this.model.authModel.get("loginToken"),
                    userId: this.model.authModel.get("userId")
                });
                this.eventCollection.fetch();
                this.listenTo(this.profModel, 'change', this.renderInfo);
                this.eventCollection.bind('add', this.populateEvents, this);
                this.eventCollection.bind('remove', this.populateEvents, this);
            },

            template: Handlebars.compile(template),

            render: function () {
                //alert(JSON.stringify(this.profModel.toJSON()));
                $(this.el).html(this.template({
                    prof: this.profModel.toJSON(),
                    amIFollowing: this.amIFollowing(),
                    followRequestSent: this.followRequestSent(),
                    showLoadMore: this.eventCount < this.eventCollection.resultCount && !this.showInfo
                }));

                //touchScroll(this.$el.find('#profContent')[0]);
                return this;
            },

            getFollowing: function() {
                Backbone.history.navigate('following/' + this.profModel.get('_id'), {
                    trigger: true
                });
            },

            getFollower: function() {
                Backbone.history.navigate('follower/' + this.profModel.get('_id'), {
                    trigger: true
                });
            },

            renderInfo: function() {
                if (this.showInfo) {
                    this.$('#profEvents').html("");
                    this.partialRender(this.template({
                        prof: this.profModel.toJSON(),
                        amIFollowing: this.amIFollowing(),
                        followRequestSent: this.followRequestSent() ,
                        showLoadMore: this.eventCount < this.eventCollection.resultCount && !this.showInfo
                    }), ['#profInfo', '#showLoadMore']);
                }
            },

            amIFollowing: function() {
                if (this.model.authModel.get('userId') == this.attributes.id)
                    return true;
                else {
                    var brandFollowing = this.model.has('brandFollowing') ? this.model.get('brandFollowing') : [];
                    var consumerFollowing = this.model.has('consumerFollowing') ? this.model.get('consumerFollowing') : [];
                    return _.contains(brandFollowing,this.attributes.id) || _.contains(consumerFollowing,this.attributes.id);
                }
            },

            followRequestSent: function() {
                var brandReq = this.model.has('brandReqSent') ? this.model.get('brandReqSent') : [];
                var consReq = this.model.has('consumerReqSent') ? this.model.get('consumerReqSent') : [];
                return _.contains(brandReq, this.attributes.id) || _.contains(consReq, this.attributes.id);
            },

            populateEvents: function() {
                if (!this.showInfo) {
                    this.$('#profEvents').html("");
                    this.$('#profInfo').html("");
                    this.eventCollection.forEach(
                        this.addEvent,
                        this
                    );
                    this.partialRender(this.template({
                        prof: this.profModel.toJSON(),
                        amIFollowing: this.amIFollowing(),
                        followRequestSent: this.followRequestSent() ,
                        showLoadMore: this.eventCount < this.eventCollection.resultCount && !this.showInfo
                    }), ['#showLoadMore']);
                }
            },

            addEvent: function(event) {
                var eventView = new eventItemView({model: event});
                this.$('#profEvents').append($(eventView.render().el).html());
            },

            toggleTab: function(event) {
                this.showInfo = event.currentTarget.id == "infoTab";
                if (this.showInfo)
                    this.renderInfo();
                else
                    this.populateEvents();
            },

            loadMore: function(event) {
                this.eventCount = this.eventCollection.resultCount;
                this.partialRender(this.template({
                    prof: this.profModel.toJSON(),
                    amIFollowing: this.amIFollowing(),
                    followRequestSent: this.followRequestSent() ,
                    showLoadMore: this.eventCount < this.eventCollection.resultCount && !this.showInfo
                }), ['#showLoadMore']);
                this.eventCollection.incrementCount();
            }
        });

        return profileView;

    });