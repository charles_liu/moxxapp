define(['handlebars'], function(Handlebars) {
    Handlebars.registerHelper('getObjFromIndex', function(array, item, index){
        if (array) {
            if (typeof index != "number")
                index = array.length-1;
            return array[index][item];
        }
    });

    Handlebars.registerHelper('numFollow', function(x,y){
        var xLength = x ? x.length : 0;
        var yLength = y? y.length : 0;
        return xLength + yLength;
    });
});