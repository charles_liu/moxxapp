//model for your user profile when logged in, empty model really
define(["jquery", "underscore", "backbone", "constants"],
    function ($, _, Backbone, C) {

        var ProfileModel = Backbone.Model.extend({
            profileId: "",
            userId: "",
            loginToken: "",

            urlRoot: constants["rootUrl"] + 'get_user/',

            url: function() {
                return this.urlRoot + this.profileId;
            },

            config: function(profileId, userId, loginToken) {
                this.profileId = profileId;
                this.userId = userId;
                this.loginToken = loginToken;
            },

            sync: function(method, model, options) {
                options.timeout = 10000;
                options.type = "POST";
                options.data = {
                    "userId": this.userId,
                    "loginToken": this.loginToken
                }
                return Backbone.sync(method, model, options);
            }
        });

        return ProfileModel;
    });