//Model for your login info
define(["jquery", "underscore", "backbone", "constants"],
    function ($, _, Backbone, C) {

        var AuthModel = Backbone.Model.extend({
            user:"",
            password:"",
            defaults: {
                "loginToken": "",
                "userId": ""
            },
            urlRoot: constants["rootUrl"] + 'login',

            url: function() {
                return this.urlRoot;
            },

            configRequest: function(user, password) {
                this.user = user;
                this.password = password;
            },

            initialize : function(options) {
                var loginDetails = localStorage.getItem('loginDetails');
                if (loginDetails != null) {
                    var loginObj = JSON.parse(loginDetails);
                    this.set({
                        "loginToken": loginObj["loginToken"],
                        "userId": loginObj["userId"]
                    });
                    this.user = loginObj["user"];
                }
            },

            hasLogin: function () {
                return localStorage.getItem('loginDetails') != null;
            },

            sync: function(method, model, options) {
                options.timeout = 10000;
                options.type = "POST";
                options.data = {
                    "password": this.password,
                    "user": this.user
                }
                return Backbone.sync(method, model, options);
            },

            parse: function(response, options) {
                if (response['success']) {
                    localStorage.setItem('loginDetails' ,JSON.stringify({"user": this.user, "loginToken": response["loginToken"], "userId": response["userId"]}));
                    this.set({
                        "loginToken": response["loginToken"],
                        "userId": response["userId"]
                    });
                }
                else {
                    localStorage.removeItem('loginDetails');
                }
            },

            logout: function() {
                localStorage.removeItem('loginDetails');
                this.clear();
            }
        });

        return AuthModel;
    });