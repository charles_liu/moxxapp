//model for your user profile when logged in
define(["jquery", "underscore", "backbone", "constants",
    "models/AuthModel"],
    function ($, _, Backbone, C, AuthModel) {

        var MyModel = Backbone.Model.extend({
            authModel: new AuthModel(),

            urlRoot: constants["rootUrl"] + 'get_user/',

            url: function() {
                return this.urlRoot + this.authModel.get('userId');
            },

            initialize : function(options) {
                this.listenTo(this.authModel, 'change', this.checkFetch);
                this.checkFetch();
            },

            checkFetch: function() {
                if (this.authModel.hasLogin())
                    this.fetch();
                else
                    this.clear();
            },

            sync: function(method, model, options) {
                options.timeout = 10000;
                options.type = "POST";
                options.data = {
                    "userId": this.authModel.get("userId"),
                    "loginToken": this.authModel.get("loginToken")
                }
                return Backbone.sync(method, model, options);
            }
        });

        return MyModel;
    });