//collection of profile models. set urlpath, userid, logintoken prior to fetching
define(["jquery", "underscore", "backbone", "constants",
    "models/ProfileModel"
],
    function($, _, Backbone, C,
             ProfileModel) {

        var ProfileCollection = Backbone.Collection.extend({
            model: ProfileModel,
            urlPath: "",
            userId: "",
            loginToken: "",

            urlRoot: constants["rootUrl"],

            url: function() {
                return this.urlRoot + this.urlPath;
            },
            configRequest: function(options) {
                this.urlPath = options.urlPath;
                this.userId = options.userId;
                this.loginToken = options.loginToken;
            },
            sync: function(method, model, options){
                options.timeout = 10000;
                options.type = "POST";
                options.data = {
                    "userId": this.userId,
                    "loginToken": this.loginToken
                }
                return Backbone.sync(method, model, options);
            },
            parse: function(response) {
                if (response["connections"]) {
                    return response["connections"];
                }
                return [];
            }
        });

        return ProfileCollection;
    });