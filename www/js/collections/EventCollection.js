//collection of profile models. set urlpath, userid, logintoken prior to fetching
define(["jquery", "underscore", "backbone", "constants",
    "models/EventModel"
],
    function($, _, Backbone, C,
             EventModel) {

        var EventCollection = Backbone.Collection.extend({
            model: EventModel,
            urlPath: "",
            userId: "",
            loginToken: "",
            count: 5,
            resultCount: 0,

            urlRoot: constants["rootUrl"],

            url: function() {
                return this.urlRoot + this.urlPath + "count=" + this.count;
            },
            configRequest: function(options) {
                this.urlPath = options.urlPath;
                this.userId = options.userId;
                this.loginToken = options.loginToken;
            },
            sync: function(method, model, options){
                options.timeout = 10000;
                options.type = "POST";
                options.data = {
                    "userId": this.userId,
                    "loginToken": this.loginToken
                }
                return Backbone.sync(method, model, options);
            },
            parse: function(response) {
                if (response["events"]) {
                    this.resultCount = response["resultCount"];
                    return response["events"];
                }
                return [];
            },

            incrementCount: function() {
                this.count += 5;
                this.fetch();
            }
        });

        return EventCollection;
    });