define(["jquery", "underscore", "backbone", "handlebars",
    "collections/ProfileCollection",
    "views/userItemView",
    "text!templates/connections.html",
    "utils/handlebarHelpers"
],
    function ($, _, Backbone, Handlebars, ProfileCollection, userItemView, template) {

        var connectionsView = Backbone.View.extend({
            toggleNum: 1,
            events: {
                "click .clickUserItem" : "clickUserItem",
                "click .toggleTab" : "clickTab"
            },

            initialize: function () {
                this.collection.fetch();
                this.collection.bind('add', this.populateConnections, this);
                this.collection.bind('remove', this.populateConnections, this);
            },

            template: Handlebars.compile(template),

            addConnection: function(connection) {
                var hasBrand = connection.get('profile').hasBrand;
                if (this.toggleNum == 1 || (hasBrand && this.toggleNum == 2) || (!hasBrand && this.toggleNum == 3)) {
                    var connectionView = new userItemView({model: connection});
                    this.$('#connections').append($(connectionView.render().el).html());
                }
            },

            render: function () {
                //alert(JSON.stringify(this.model.attributes));
                $(this.el).html(this.template({}));
                this.populateConnections();
                return this;
            },

            populateConnections: function() {
                this.$('#connections').html("");
                this.collection.forEach(
                    this.addConnection,
                    this
                );
            },

            clickUserItem: function(event) {
                Backbone.history.navigate('profile/' + event.currentTarget.id, {
                    trigger: true
                });
            },

            clickTab: function(event) {
                this.toggleNum = event.currentTarget.id;
                this.populateConnections();
            }
        });

        return connectionsView;

    });