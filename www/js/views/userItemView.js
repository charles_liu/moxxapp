define(["jquery", "underscore", "backbone", "handlebars",
    "text!templates/userItem.html",
    "utils/handlebarHelpers"
],
    function ($, _, Backbone, Handlebars, template) {

        var userItemView = Backbone.View.extend({
            events: {
            },

            template: Handlebars.compile(template),

            render: function () {
                //alert(JSON.stringify(this.model.attributes));
                $(this.el).html(this.template({user: this.model.toJSON()}));
                return this;
            }
        });

        return userItemView;

    });