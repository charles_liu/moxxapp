define(["jquery", "underscore", "backbone", "handlebars",
    "models/MyModel",
    "text!templates/home.html",
    "collections/EventCollection",
    "views/eventItemView",
    "utils/handlebarHelpers"
  ],
  function($, _, Backbone, Handlebars,
    MyModel,
    template, EventCollection, eventItemView) {

    var homeView = Backbone.View.extend({
      eventCount: 0,
      initialize: function() {
        // here we can register to inTheDOM or removing events
        //this.listenTo(this, "inTheDOM", functionName);
        // this.listenTo(this, "removing", functionName);

        // by convention, all the inner views of a view must be stored in this.subViews
          this.eventCollection = new EventCollection();
          this.eventCollection.configRequest({
              urlPath: "getHome/",
              loginToken: this.model.authModel.get("loginToken"),
              userId: this.model.authModel.get("userId")
          });
          this.eventCollection.fetch();
          this.eventCollection.bind('add', this.populateEvents, this);
          this.eventCollection.bind('remove', this.populateEvents, this);
      },

      template: Handlebars.compile(template),

      events: {
        // "touchend #node_id": "handler"
          "click #loadMore" : "loadMore"
      },

      render: function() {
        $(this.el).html(this.template({
            showLoadMore: this.eventCount < this.eventCollection.resultCount}));
        return this;
      },

        populateEvents: function() {
            this.$('#homeEvents').html("");
            this.eventCollection.forEach(
                this.addEvent,
                this
            );
            this.partialRender(this.template({
                showLoadMore: this.eventCount < this.eventCollection.resultCount
            }), ['#showLoadMore']);
        },

        addEvent: function(event) {
            var eventView = new eventItemView({model: event});
            this.$('#homeEvents').append($(eventView.render().el).html());
        },

        loadMore: function(event) {
            this.eventCount = this.eventCollection.resultCount;
            this.partialRender(this.template({
                showLoadMore: this.eventCount < this.eventCollection.resultCount && !this.showInfo
            }), ['#showLoadMore']);
            this.eventCollection.incrementCount();
        }
    });

    return homeView;

  });