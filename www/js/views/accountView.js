define(["jquery", "underscore", "backbone", "handlebars",
    "text!templates/account.html"
  ],
  function($, _, Backbone, Handlebars,
    template) {

    var accountView = Backbone.View.extend({

      id: "profileView",

      events: {
      },

      initialize: function(options) {
      },

      template: Handlebars.compile(template),

      render: function() {
        $(this.el).html(this.template({}));
        return this;
      }
    });

    return accountView;

  });