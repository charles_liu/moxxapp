define(["jquery", "underscore", "backbone", "handlebars",
    "text!templates/eventItem.html",
    "utils/handlebarHelpers"
],
    function ($, _, Backbone, Handlebars, template) {

        var eventItemView = Backbone.View.extend({
            events: {
            },

            template: Handlebars.compile(template),

            render: function () {
                //alert(JSON.stringify(this.model.attributes));
                $(this.el).html(this.template({event: this.model.toJSON()}));
                return this;
            }
        });

        return eventItemView;

    });